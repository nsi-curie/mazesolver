import random
import sys

import numpy as np

sys.setrecursionlimit(10000)

movements = []


def border(cells):
    # Fonction pour ajouter une bordure protectrice pour éviter de sortir des limites
    lines = len(cells)
    cells.insert(0, ["#"] * (lines + 2))

    for i in range(1, lines + 1):
        cells[i].insert(0, "#")
        cells[i].append("#")

    cells.append(["#"] * (lines + 2))

    return cells


def progress(carreau, cells, end):
    global movements
    movements = []
    mov_sortie = []
    sortie_atteinte = None
    max_visited = 0

    # Nombre de cases max, pour éviter une boucle infinie
    while max_visited != len(cells) ** 2:
        max_visited += 1
        new_visited_case = []
        new_visited_case_last_step = []

        for carre in carreau:
            x = carre[1]
            y = carre[0]

            # detecter si la case présente est la sortie
            if x == end[1] and y == end[0]:
                sortie_atteinte = True

            # explore les alentours de chaque cases (bas,haut,gauche,droite)
            if cells[y - 1][x] == 0 or cells[y - 1][x] == 3:
                new_visited_case.append([y - 1, x])
                new_visited_case_last_step.append([y, x])
                cells[y - 1][x] = 1

            if cells[y + 1][x] == 0 or cells[y + 1][x] == 3:
                new_visited_case.append([y + 1, x])
                new_visited_case_last_step.append([y, x])
                cells[y + 1][x] = 1

            if cells[y][x + 1] == 0 or cells[y][x + 1] == 3:
                new_visited_case.append([y, x + 1])
                new_visited_case_last_step.append([y, x])
                cells[y][x + 1] = 1

            if cells[y][x - 1] == 0 or cells[y][x - 1] == 3:
                new_visited_case.append([y, x - 1])
                new_visited_case_last_step.append([y, x])
                cells[y][x - 1] = 1

        carreau = new_visited_case
        movements.append(new_visited_case)
        mov_sortie.append(new_visited_case_last_step)
        if sortie_atteinte:
            break

    if sortie_atteinte:
        del movements[-1]
        del mov_sortie[-1]
        return mov_sortie, movements, True

    else:
        return [], movements, False


def sortie(mov_sortie, start, end):
    global movements

    exit_path = []
    i = 1
    last_step = end

    # on recule dans nos mouvements de la sortie à l'entrée
    while last_step != start:
        h = last_step
        step = movements[-i].index(last_step)
        last_step = mov_sortie[-i][step]

        # pour charger les textures directionnel avec des mots clés
        y_difference = last_step[0] - h[0]
        x_difference = last_step[1] - h[1]
        if y_difference == -1:
            f = "down"
        elif y_difference == 1:
            f = "up"
        elif x_difference == -1:
            f = "right"
        elif x_difference == 1:
            f = "left"
        last_mouvement = [last_step, f]

        exit_path.insert(0, last_mouvement)
        i += 1

    del exit_path[0]

    return exit_path


def solve(cells, start, end):
    global movements

    # permet d'ajouter une sourcouche pour eviter les problemes d'index
    # quand on est au bord du labyrinthe et qu'on parcours les cases à coté
    cells = border(cells)

    end_y = end[0] + 1
    end_x = end[1] + 1
    end = [end_y, end_x]

    start_x = start[1] + 1
    start_y = start[0] + 1
    start = [start_y, start_x]

    # liste des cases que l'on parcours à commencer par l'entree
    carreau = [start]

    mov_sortie, movements, check = progress(carreau, cells, end)

    # si l'entree et la sortie se rejoignent on vient chercher
    # le parcours de sortie à partir des mouvements effectué à reculons
    if check:
        path = sortie(mov_sortie, start, end)

        step = movements[-1].index(end)
        del movements[-1][step]
        return movements, path

    else:
        path = []
        return movements, path


def random_maze(size):
    # permet de creer un labyrinthe avec un seul chemin
    def chemin_principaux(size):
        cells = np.ones((size, size), dtype=int)

        def acces_nouvelle_cellule(cell):
            cells[cell] = 0
            directions = [(0, 1), (1, 0), (0, -1), (-1, 0)]
            random.shuffle(directions)

            for mov_y, mov_x in directions:
                nouvelle_cellume = (cell[0] + 2 * mov_x, cell[1] + 2 * mov_y)

                if 0 <= nouvelle_cellume[0] < size and 0 <= nouvelle_cellume[1] < size and cells[nouvelle_cellume] == 1:
                    cells[cell[0] + mov_x, cell[1] + mov_y] = 0
                    acces_nouvelle_cellule(nouvelle_cellume)

        depart = (0, 0)
        acces_nouvelle_cellule(depart)

        return cells

    # En percant certaines cases aleatoirement on vient creer d'autre chemins d'acces
    def chemin_second(cells):
        size = len(cells)
        n = 0
        cells = np.pad(cells, ((1, 1), (1, 1)), constant_values=4)

        while n != int(0.5*int(size)-1):

            y = random.randint(1, size)
            x = random.randint(1, size)

            c = True
            if y == size:
                if cells[y-1, x] == 1:
                    c = False

            if x == size:
                if cells[y, x-1] == 1:
                    c = False

            if cells[y, x] == 1 and c:
                if cells[y-1, x] == 1 and cells[y+1, x] == 1:
                    n += 1
                    cells[y, x] = 0

                elif cells[y, x-1] == 1 and cells[y, x+1] == 1:
                    n += 1
                    cells[y, x] = 0

        cells = cells[1:-1, 1:-1]
        return cells

    cells = chemin_principaux(size)

    cells = chemin_second(cells)
    cells[0, 0] = 2  # entrée
    cells[-1, -2] = 0  # garantit qu'une case autour de l'entree est vide
    cells[-1, -1] = 3  # sortie
    return cells
