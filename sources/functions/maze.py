import math
from time import sleep
from tkinter import Button, filedialog, messagebox

from customtkinter import CTkCanvas

from functions import sidebar, solver, window

colors = [
    "#FFFFFF",
    "#222831",
    "#00FF00",
    "#FF0000",
]

stateTextures = ["empty", "wall", "start", "end", "progress",
                 "path-up",
                 "path-right",
                 "path-down",
                 "path-left"]


canvas = CTkCanvas(window.root)
paused = False
cells = []
starts = []
ends = []
solving = False
solved = False
solving_speed = 0.4
canvas_size = 0
grid_size = 0
frame_size = 0
loss = 0

game_started = False
player_movements = []


def init(size: int, real_size: int, canvas_x: int = 0, canvas_y: int = 0):
    global cells, canvas, canvas_size, grid_size

    grid_size = size
    canvas_size = real_size

    canvas.configure(width=canvas_size, height=canvas_size)
    canvas.pack()
    canvas.place(x=canvas_x, y=canvas_y)

    cells = [
        [
            {
                "state": 0,
                "widget": None,
            }
            for _ in range(grid_size)
        ]
        for _ in range(grid_size)
    ]

    resize(grid_size)


def on_cell_click(x: int, y: int):
    # Permet de changer l'etat de la case par un simple clic
    def callback(_):
        if game_started:
            return
        global cells, starts, ends, solving, solved

        if solving:
            return

        if solved:
            solved = False

            # Remettre l'image de base sur toutes les cases vides

            for _, x1 in enumerate(range(len(cells))):
                for _, y1 in enumerate(range(len(cells[x]))):
                    if cells[x1][y1]["state"] == 0:
                        cells[x1][y1]["widget"].configure(
                            image=get_cell_texture(x1, y1, "empty")
                        )

        cells[x][y]["state"] = (cells[x][y]["state"] + 1) % 4

        # Début / Fin
        if [x, y] in starts:
            starts.remove([x, y])

        if [x, y] in ends:
            ends.remove([x, y])

        if cells[x][y]["state"] == 2:
            starts.append([x, y])

        if cells[x][y]["state"] == 3:
            ends.append([x, y])

        # Couleur
        cells[x][y]["widget"].configure(
            image=get_cell_texture(x, y, stateTextures[cells[x][y]["state"]])
        )

    return callback


def get_cell_texture(x: int, y: int, texture: str):
    global cells

    if x < 0 or y < 0 or x >= grid_size or y >= grid_size:
        return window.textures["empty"]
      
    return window.textures[texture]


def pause(state=True, gui_update=True):
    global paused

    paused = state

    if gui_update:
        if paused:
            sidebar.solveButton.configure(
                image=window.textures["play"], text="Reprendre"
            )
        else:
            sidebar.solveButton.configure(
                image=window.textures["pause"], text="Pause")


def speed(new_speed):
    global solving_speed

    real_speed = [0.8, 0.4, 0.3, 0.2]
    label_speed = [0.5, 1, 1.5, 2]
    solving_speed = real_speed[int(new_speed)]

    sidebar.speedSliderLabel.configure(
        text=f"Vitesse: {label_speed[int(new_speed)]}x")


def resize(new_size):
    global cells, starts, ends, grid_size, canvas_size, frame_size, solving, solved, loss,game_started

    grid_size = int(new_size)
    game_started=False

    # Réinitialiser la grille
    canvas.delete("all")
    solving = False
    solved = False
    pause(False, False)

    sidebar.sizeSlider.configure(state="disabled")
    sidebar.generateButton.configure(state="disabled")
    sidebar.importButton.configure(state="disabled")
    sidebar.exportButton.configure(state="disabled")
    sidebar.sizeButton.configure(state="disabled")
    sidebar.personalizeButton.configure(state="disabled")

    # Changer la taille de la liste en mémoire
    if grid_size > len(cells):
        for _ in range(grid_size - len(cells)):
            cells.append(
                [
                    {
                        "state": 0,
                        "widget": None,
                    }
                    for _ in range(grid_size)
                ]
            )
    elif grid_size < len(cells):
        for _ in range(len(cells) - grid_size):
            cells.pop()

    for x in range(grid_size):
        if grid_size > len(cells[x]):
            for _ in range(grid_size - len(cells[x])):
                cells[x].append(
                    {
                        "state": 0,
                        "widget": None,
                    }
                )
        elif grid_size < len(cells[x]):
            for _ in range(len(cells[x]) - grid_size):
                cells[x].pop()

    # Retirer les débuts/fins en dehors de la grille
    starts = [start for start in starts if start[0]
              < grid_size and start[1] < grid_size]
    ends = [end for end in ends if end[0] < grid_size and end[1] < grid_size]

    # Dessiner la grille
    frame_size = math.floor(canvas_size / grid_size)
    window.resize(300 + frame_size*grid_size, frame_size*grid_size)

    window.load_maze_textures(size=frame_size)

    for x in range(grid_size):
        for y in range(grid_size):
            if cells[x][y]["widget"] is not None:
                cells[x][y]["widget"].destroy()

            cell_width = frame_size 
            cell_height = frame_size 
            cell_x = y * frame_size 
            cell_y = x * frame_size 

            cells[x][y]["widget"] = Button(
                canvas,
                image=get_cell_texture(
                    x, y, stateTextures[cells[x][y]["state"]]),
                width=cell_width,
                height=cell_height,
                highlightthickness=0,
                bd=0,
                relief="solid",
            )

            canvas.create_window(
                cell_x,
                cell_y,
                width=cell_width,
                height=cell_height,
                anchor="nw",
                window=cells[x][y]["widget"],
            )

            cells[x][y]["widget"].bind("<Button-1>", on_cell_click(x, y))
    
    sidebar.sizeSlider.configure(state="normal")
    sidebar.generateButton.configure(state="normal")
    sidebar.importButton.configure(state="normal")
    sidebar.exportButton.configure(state="normal")
    sidebar.sizeButton.configure(state="normal")
    sidebar.personalizeButton.configure(state="normal")
      

def solve():
    global cells, starts, ends, solving, solved, paused, solving_speed,game_started

    easter = [[0, 0], [len(cells)-1, 0], [0, len(cells)-1],
              [len(cells)-1, len(cells)-1]]
    if easter == starts:
        messagebox.showerror(
            'Congratulation', "You are a genius contact us on discord :\n   lorenzialex and natoune_")
    game_started=False
    # permet de mettre en pause
    if solving:
        pause(not paused)
        return

    # verifier les conditions entree/sortie
    if len(starts) != 1 or len(ends) != 1:
        messagebox.showerror(
            "Erreur", "Vous devez placer un début et une fin !")
        return

    solving = True
    solved = False
    pause(False)

    # on desactive les boutons pour eviter de surcharger de fonctions
    sidebar.sizeSlider.configure(state="disabled")
    sidebar.generateButton.configure(state="disabled")
    sidebar.importButton.configure(state="disabled")
    sidebar.exportButton.configure(state="disabled")
    sidebar.sizeButton.configure(state="disabled")
    sidebar.solveButton.configure(image=window.textures["pause"], text="Pause")

    for x, _ in enumerate(cells):
        for y, _ in enumerate(cells[x]):
            if cells[x][y]["state"] == 0:
                cells[x][y]["widget"].configure(
                    image=get_cell_texture(x, y, "empty"))

    start = starts[-1]
    end = ends[-1]

    state_cells = [[0 for _ in range(grid_size)] for _ in range(grid_size)]

    # recuperer dans un tableau de valeur la value de chaque case
    # for x in range(len(cells)):
    for x, _ in enumerate(cells):
        # for y in range(len(cells[x])):
        for y, _ in enumerate(cells[x]):
            if cells[x][y]["state"] > 3:
                cells[x][y]["state"] = 0
                cells[x][y]["widget"].configure(
                    image=get_cell_texture(x, y, "empty"))
            state_cells[x][y] = cells[x][y]["state"]

    explored, solution = solver.solve(state_cells, start, end)

    # Permet d'afficher la phase d'exploration du labyrinthe
    for step in explored:
        while paused and solving:
            window.root.update()

        for move in step:
            if solving:
                cells[move[0] - 1][move[1] - 1]["widget"].configure(
                    image=get_cell_texture(
                        move[0] - 1, move[1] - 1, "progress")
                )
                cells[move[0] - 1][move[1] - 1]["state"] = 4

        window.root.update()
        sleep(solving_speed)

    # Permet d'afficher le chemin de sortie
    for move in solution:
        while paused and solving:
            window.root.update()

        if solving:
            cells[move[0][0] - 1][move[0][1] - 1]["widget"].configure(
                image=get_cell_texture(
                    move[0][0] - 1, move[0][1] - 1, "path-" + move[1])
            )
            cells[move[0][0] - 1][move[0][1] -
                                  1]["state"] = stateTextures.index("path-" + move[1])

        window.root.update()
        sleep(solving_speed / 2)

    # Fin on reactive les fonctions
    solving = False
    solved = True
    pause(False)

    sidebar.sizeSlider.configure(state="normal")
    sidebar.generateButton.configure(state="normal")
    sidebar.importButton.configure(state="normal")
    sidebar.exportButton.configure(state="normal")
    sidebar.sizeButton.configure(state="normal")
    sidebar.solveButton.configure(
        image=window.textures["play"], text="Résoudre")


def reset():
    # toutes les variables d'etat ainsi que les images sont remises à leur etat d'origine
    global cells, starts, ends, solving, solved, player_movements,game_started

    solving = False
    game_started=False
    solved = False
    pause(False, False)

    sidebar.sizeSlider.configure(state="disabled")
    sidebar.generateButton.configure(state="disabled")
    sidebar.importButton.configure(state="disabled")
    sidebar.exportButton.configure(state="disabled")
    sidebar.sizeButton.configure(state="disabled")
    sidebar.personalizeButton.configure(state="disabled")

    sidebar.solveButton.configure(
        image=window.textures["play"], text="Résoudre")

    cells = [
        [
            {
                "state": 0,
                "widget": None,
            }
            for _ in range(grid_size)
        ]
        for _ in range(grid_size)
    ]

    starts = []
    ends = []

    resize(grid_size)

    sidebar.solveButton.configure(
        image=window.textures["play"], text="Résoudre")

    sidebar.sizeSlider.configure(state="normal")
    sidebar.generateButton.configure(state="normal")
    sidebar.importButton.configure(state="normal")
    sidebar.exportButton.configure(state="normal")
    sidebar.sizeButton.configure(state="normal")
    sidebar.personalizeButton.configure(state="normal")


def generate():
    global cells, starts, ends, grid_size, player_movements,game_started

    sidebar.sizeSlider.configure(state="disabled")
    sidebar.generateButton.configure(state="disabled")
    sidebar.importButton.configure(state="disabled")
    sidebar.exportButton.configure(state="disabled")
    sidebar.sizeButton.configure(state="disabled")
    sidebar.personalizeButton.configure(state="disabled")

    reset()

    game_started=False
    generated_cells = solver.random_maze(grid_size)

    # placer les textures
    for y, _ in enumerate(generated_cells):
        for x, _ in enumerate(generated_cells[y]):

            cells[x][y]["state"] = int(generated_cells[x][y])

            # Début / Fin
            if cells[x][y]["state"] == 2:
                starts.append([x, y])
                player_movements = [tuple(starts[0])]
            if cells[x][y]["state"] == 3:
                ends.append([x, y])

            cells[x][y]["widget"].config(
                image=get_cell_texture(
                    x, y, stateTextures[cells[x][y]["state"]])
            )
    
    sidebar.sizeSlider.configure(state="normal")
    sidebar.generateButton.configure(state="normal")
    sidebar.importButton.configure(state="normal")
    sidebar.exportButton.configure(state="normal")
    sidebar.sizeButton.configure(state="normal")
    sidebar.personalizeButton.configure(state="normal")


def import_file():
    global cells, starts, ends, sizeSlider, player_movements,game_started
    
    sidebar.sizeSlider.configure(state="disabled")
    sidebar.generateButton.configure(state="disabled")
    sidebar.importButton.configure(state="disabled")
    sidebar.exportButton.configure(state="disabled")
    sidebar.sizeButton.configure(state="disabled")
    sidebar.personalizeButton.configure(state="disabled")

    file = filedialog.askopenfilename(
        defaultextension=".maze",
        filetypes=[("Labyrinthe", "*.maze"), ("Tous les fichiers", "*.*")],
    )
    # Le fichier est sélectionné
    if file:
        with open(file, "r", encoding="utf-8") as file:
            lines = file.readlines()

            # Verifier la conformité de l'import
            if len(lines[0])-1 != len(lines):
                messagebox.showerror(
                    'Erreur', "Le labyrinthe doit etre carré et non rectangulaire")

            else:
                # Slider
                sidebar.sizeSliderLabel.configure(
                    text=f"Taille de la grille: {int(len(lines))}x{int(len(lines))}"
                )
                sidebar.sizeSlider.set(len(lines))

                resize(len(lines))

                # Retirer les débuts/fins
                starts = []
                ends = []

                # Remplir la grille
                for x in range(len(lines)):
                    lines[x] = lines[x][0:-1]
                    for y in range(len(lines)):
                        cells[x][y]["state"] = int(lines[x][y])

                        # Début / Fin
                        if cells[x][y]["state"] == 2:
                            starts.append([x, y])
                            player_movements = [tuple(starts[0])]

                        if cells[x][y]["state"] == 3:
                            ends.append([x, y])

                        # Couleur
                        cells[x][y]["widget"].configure(
                            image=get_cell_texture(
                                x, y, stateTextures[cells[x][y]["state"]])
                        )
                game_started=False
    
    sidebar.sizeSlider.configure(state="normal")
    sidebar.generateButton.configure(state="normal")
    sidebar.importButton.configure(state="normal")
    sidebar.exportButton.configure(state="normal")
    sidebar.sizeButton.configure(state="normal")
    sidebar.personalizeButton.configure(state="normal")


def export_file():
    global cells,game_started,player_movements
    
    sidebar.sizeSlider.configure(state="disabled")
    sidebar.generateButton.configure(state="disabled")
    sidebar.importButton.configure(state="disabled")
    sidebar.exportButton.configure(state="disabled")
    sidebar.sizeButton.configure(state="disabled")
    sidebar.personalizeButton.configure(state="disabled")

    file = filedialog.asksaveasfilename(
        defaultextension=".maze",
        filetypes=[("Fichiers maze", "*.maze"), ("Tous les fichiers", "*.*")],
    )

    # Le fichier est sélectionné
    if file:
        with open(file, "w", encoding="utf-8") as file:


            # ecrire dans un fichier la value de chaque case
            if game_started:
                         
             for x, _ in enumerate(cells):
                for y, _ in enumerate(cells[x]):

                    if player_movements[0][0]==x and player_movements[0][1]==y:
                        file.write('2')

                    elif cells[x][y]["state"]<4 and cells[x][y]["state"]!=2:
                     file.write(str(cells[x][y]["state"]))

                    else:
                     file.write('0')

                file.write("\n")

            else:
                for x, _ in enumerate(cells):
                 for y, _ in enumerate(cells[x]):
                     
                     file.write(str(cells[x][y]["state"]))

                 file.write("\n")
    
    sidebar.sizeSlider.configure(state="normal")
    sidebar.generateButton.configure(state="normal")
    sidebar.importButton.configure(state="normal")
    sidebar.exportButton.configure(state="normal")
    sidebar.sizeButton.configure(state="normal")
    sidebar.personalizeButton.configure(state="normal")


def personalize():
    etat=sidebar.sizeSlider.cget('state')
    sidebar.sizeSlider.configure(state="disabled")
    sidebar.generateButton.configure(state="disabled")
    sidebar.importButton.configure(state="disabled")
    sidebar.exportButton.configure(state="disabled")
    sidebar.sizeButton.configure(state="disabled")
    sidebar.personalizeButton.configure(state="disabled")

    global cells, frame_size
    window.personalize1(frame_size)
    for y in range(len(cells)):
        for x in range(len(cells[y])):
            cells[x][y]["widget"].config(
                image=get_cell_texture(
                    x, y, stateTextures[cells[x][y]["state"]])
            )
    sidebar.sizeSlider.configure(state=etat)
    sidebar.generateButton.configure(state=etat)
    sidebar.importButton.configure(state=etat)
    sidebar.exportButton.configure(state=etat)
    sidebar.sizeButton.configure(state=etat)
    sidebar.personalizeButton.configure(state="normal")


def remise_zero():
    global player_movements, cells, game_started
    cells[player_movements[0][0]][player_movements[0][1]]["state"] = 2
    cells[player_movements[0][0]][player_movements[0][1]]["widget"].configure(
        image=get_cell_texture(player_movements[0][0], player_movements[0][1], "start"))
    for i in range(1, len(player_movements)):
        cells[player_movements[i][0]][player_movements[i][1]]["state"] = 0
        cells[player_movements[i][0]][player_movements[i][1]]["widget"].configure(
            image=get_cell_texture(player_movements[i][0], player_movements[i][1], "empty"))
    starts[0] = list(player_movements[0])
    player_movements = []
    game_started = False


def on_key_press(key):
    global game_started, player_movements, cells

    if hasattr(key, "name") and ["down", "up", "left", "right"].count(key.name) > 0 and len(starts) == 1 and len(ends) == 1:

        if len(player_movements) == 0:
            player_movements = [tuple(starts[0])]


        if key.name == "down":
            game_started = True

            if starts[0][0] == grid_size-1:
                return

            if cells[starts[0][0]+1][starts[0][1]]["state"] == 1:
                return

            if cells[starts[0][0]+1][starts[0][1]]["state"] > 4:
                messagebox.showerror(
                    "Game Over", "Vous avez perdu ! Retour à la case départ")
                remise_zero()
                return

            if cells[starts[0][0]+1][starts[0][1]]["state"] == 3:
                messagebox.showinfo(
                    "Congratulations", "Vous avez trouvé la sortie ! Félicitations !")
                remise_zero()
                return

            cells[starts[0][0]][starts[0][1]]["state"] = 7
            cells[starts[0][0]][starts[0][1]]["widget"].configure(
                image=get_cell_texture(starts[0][0], starts[0][1], "path-down"))

            starts[0][0] += 1

            cells[starts[0][0]][starts[0][1]]["state"] = 2
            cells[starts[0][0]][starts[0][1]]["widget"].configure(
                image=get_cell_texture(starts[0][0], starts[0][1], "start"))
            player_movements.append(tuple(starts[0]))

        if key.name == "up":
            game_started = True

            if starts[0][0] == 0:
                return

            if cells[starts[0][0]-1][starts[0][1]]["state"] == 1:
                return

            if cells[starts[0][0]-1][starts[0][1]]["state"] > 4:
                messagebox.showerror(
                    "Game Over", "Vous avez perdu ! Retour à la case départ")
                remise_zero()
                return

            if cells[starts[0][0]-1][starts[0][1]]["state"] == 3:
                messagebox.showinfo(
                    "Congratulations", "Vous avez trouvé la sortie ! Félicitations !")
                remise_zero()
                return

            cells[starts[0][0]][starts[0][1]]["state"] = 5
            cells[starts[0][0]][starts[0][1]]["widget"].configure(
                image=get_cell_texture(starts[0][0], starts[0][1], "path-up"))

            starts[0][0] -= 1

            cells[starts[0][0]][starts[0][1]]["state"] = 2
            cells[starts[0][0]][starts[0][1]]["widget"].configure(
                image=get_cell_texture(starts[0][0], starts[0][1], "start"))
            player_movements.append(tuple(starts[0]))

        if key.name == "left":
            game_started = True

            if starts[0][1] == 0:
                return

            if cells[starts[0][0]][starts[0][1]-1]["state"] == 1:
                return

            if cells[starts[0][0]][starts[0][1]-1]["state"] > 4:
                messagebox.showerror(
                    "Game Over", "Vous avez perdu ! Retour à la case départ")
                remise_zero()
                return

            if cells[starts[0][0]][starts[0][1]-1]["state"] == 3:
                messagebox.showinfo(
                    "Congratulations", "Vous avez trouvé la sortie ! Félicitations !")
                remise_zero()
                return

            cells[starts[0][0]][starts[0][1]]["state"] = 8
            cells[starts[0][0]][starts[0][1]]["widget"].configure(
                image=get_cell_texture(starts[0][0], starts[0][1], "path-left"))

            starts[0][1] -= 1

            cells[starts[0][0]][starts[0][1]]["state"] = 2
            cells[starts[0][0]][starts[0][1]]["widget"].configure(
                image=get_cell_texture(starts[0][0], starts[0][1], "start"))
            player_movements.append(tuple(starts[0]))

        if key.name == "right":
            game_started = True

            if starts[0][1] == grid_size-1:
                return

            if cells[starts[0][0]][starts[0][1]+1]["state"] == 1:
                return

            if cells[starts[0][0]][starts[0][1]+1]["state"] > 4:
                messagebox.showerror(
                    "Game Over", "Vous avez perdu ! Retour à la case départ")
                remise_zero()
                return

            if cells[starts[0][0]][starts[0][1]+1]["state"] == 3:
                messagebox.showinfo(
                    "Congratulations", "Vous avez trouvé la sortie ! Félicitations !")
                remise_zero()
                return

            cells[starts[0][0]][starts[0][1]]["state"] = 6
            cells[starts[0][0]][starts[0][1]]["widget"].configure(
                image=get_cell_texture(starts[0][0], starts[0][1], "path-right"))

            starts[0][1] += 1

            cells[starts[0][0]][starts[0][1]]["state"] = 2
            cells[starts[0][0]][starts[0][1]]["widget"].configure(
                image=get_cell_texture(starts[0][0], starts[0][1], "start"))
            player_movements.append(tuple(starts[0]))
