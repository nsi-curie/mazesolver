import os
from sys import exit as sys_exit

from customtkinter import CTk, CTkCanvas, CTkImage, set_appearance_mode
from PIL import Image, ImageTk

root = CTk()
canvas = CTkCanvas(root)
window_width = 0
window_height = 0
textures = {}
texture_key = 0
texture_packs = ['garden.png',
                 'castle.png',
                 'volcano.png',
                 'ice_floe.png',
                 'neighbourhood.png']


def init():
    global window_width, window_height, canvas, root, texture_key, texture_packs

    root.title("Maze Solver")

    # Icone
    if os.name == "nt":
        root.iconbitmap(os.path.join(
            os.path.dirname(__file__), "../assets/icon.ico"))

    root.iconphoto(
        True,
        ImageTk.PhotoImage(
            Image.open(os.path.join(os.path.dirname(
                __file__), "../assets/icon.png"))
        ),
    )

    # Taille / position fenêtre
    # resize(root.winfo_screenwidth() / 2, root.winfo_screenheight() / 2)
    root.resizable(False, False)

    # Thème
    set_appearance_mode("system")

    # Canvas
    canvas.configure(width=window_width, height=window_height)
    canvas.pack()
    canvas.place(x=0, y=0)

    # Textures
    load_default_textures()
    load_maze_textures()

    # Quitter
    root.protocol("WM_DELETE_WINDOW", close)


def close():
    global root

    root.destroy()
    sys_exit(0)


def resize(width, height):
    global window_width, window_height

    window_width = int(width)
    window_height = int(height)

    center_x = int(root.winfo_screenwidth() / 2 - width / 2)
    center_y = int(root.winfo_screenheight() / 2 - height / 2)

    root.geometry(f"{window_width}x{window_height}+{center_x}+{center_y}")

# precharger les textures


def load_default_textures():
    global textures

    image_paths = {
        "play": "assets/gui/play.png",
        "pause": "assets/gui/pause.png",
        "size": "assets/gui/size.png",
        "reset": "assets/gui/reset.png",
        "generate": "assets/gui/generate.png",
        "import": "assets/gui/import.png",
        "export": "assets/gui/export.png",
        "personalize": "assets/gui/personalize.png",
    }

    for texture_name, texture_path in image_paths.items():
        image = Image.open(texture_path)
        textures[texture_name] = CTkImage(image)


def personalize1(frame_size):
    global texture_key, texture_packs
    texture_key = (texture_key+1) % len(texture_packs)
    load_maze_textures(size=frame_size)


def load_maze_textures(
    size: int = 50,
    empty: str = "assets/textures/empty",
    wall: str = "assets/textures/wall",
    start: str = "assets/textures/start",
    end: str = "assets/textures/end",
    progress: str = "assets/textures/progress",
    path_up: str = "assets/textures/path/up",
    path_right: str = "assets/textures/path/right",
    path_down: str = "assets/textures/path/down",
    path_left: str = "assets/textures/path/left",
):
    global textures, texture_key, texture_packs

    image_paths = {
        "empty": empty,
        "wall": wall,
        "start": start,
        "end": end,
        "progress": progress,
        "path-up": path_up,
        "path-right": path_right,
        "path-down": path_down,
        "path-left": path_left,
    }

    # for texture in image_paths:
    for texture_name, texture_path in image_paths.items():
        texture_path = os.path.join(texture_path, texture_packs[texture_key])
        image = Image.open(texture_path)
        image = image.resize((size, size), Image.AFFINE)
        image2 = image.copy()
        image2 = image2.resize((size + 1, size + 1), Image.AFFINE)

        textures[texture_name] = ImageTk.PhotoImage(image)
        textures[texture_name + "+1"] = ImageTk.PhotoImage(image2)
