from customtkinter import CTkButton, CTkCanvas, CTkFrame, CTkLabel, CTkSlider

from functions import maze, window

sidebar_width = 0
grid_size = 20

canvas = CTkCanvas(window.root)
frame = CTkFrame(canvas)

title = CTkLabel(frame)

sizeSliderLabel = CTkLabel(frame)
sizeSlider = CTkSlider(frame)
sizeButton = CTkButton(frame)

speedSliderLabel = CTkLabel(frame)
speedSlider = CTkSlider(frame)

solveButton = CTkButton(frame)
resetButton = CTkButton(frame)
generateButton = CTkButton(frame)

importFrame = CTkFrame(frame)
importButton = CTkButton(importFrame)
exportButton = CTkButton(importFrame)

personalizeButton = CTkButton(frame)


def init(width: int = 200):
    global grid_size, sidebar_width

    sidebar_width = width

    canvas.configure(
        width=sidebar_width,
        height=window.window_height,
        highlightthickness=0,
    )
    canvas.pack(side="left", fill="y")
    canvas.place(x=0, y=0, width=sidebar_width, height=window.window_height)

    # Title
    title.configure(text="Maze Solver", font=("Arial", 20, "bold"))
    title.pack(side="top", fill="x", pady=(40, 25))

    # Create a frame which take all the space in the sidebar
    frame.pack(side="top", fill="both", expand=True)

    # Size slider
    sizeSliderLabel.configure(text="Taille de la grille: 20x20")
    sizeSliderLabel.pack(side="top", fill="x")

    sizeSlider.configure(
        from_=6,
        to=40,
        number_of_steps=34,
        width=sidebar_width - 50,
        command=on_resize_slider,
    )
    sizeSlider.set(20)
    sizeSlider.pack(side="top", padx=(10, 0))

    sizeButton.configure(
        fg_color="#003049",
        hover_color="#000000",
        text="Redimensionner la grille",
        command=resize,
        image=window.textures["size"],
    )

    sizeButton.pack(side="top", pady=(10, 0))

    maze.sizeSlider = sizeSlider

    # Speed slider
    speedSliderLabel.configure(text="Vitesse: 1x")
    speedSliderLabel.pack(side="top", fill="x", pady=(25, 0))

    speedSlider.configure(
        from_=0,
        to=3,
        number_of_steps=3,
        width=sidebar_width - 20,
        command=maze.speed
    )
    speedSlider.set(1)
    speedSlider.pack(side="top")

    # Solve button
    solveButton.configure(
        text="Résoudre",
        font=("Arial", 15),
        width=sidebar_width - 20,
        command=maze.solve,
        image=window.textures["play"],
    )
    solveButton.pack(side="top", pady=(25, 10))

    # Reset button
    resetButton.configure(
        text="Réinitialiser",
        font=("Arial", 15),
        width=sidebar_width - 20,
        fg_color="#D62828",
        hover_color="#B21F1F",
        image=window.textures["reset"],
        command=maze.reset,
    )
    resetButton.pack(side="top", pady=5)

    # Generate button
    generateButton.configure(
        text="Générer un labyrinthe",
        font=("Arial", 15),
        width=sidebar_width - 20,
        fg_color="#2C6E49",
        hover_color="#1F5C3D",
        image=window.textures["generate"],
        command=maze.generate,
    )
    generateButton.pack(side="top", pady=(25, 5))

    # Import frame
    importFrame.configure(fg_color="transparent")
    importFrame.pack(side="bottom", fill="x", pady=(0, 25))

    # Import button
    importButton.configure(
        text="Importer",
        font=("Arial", 15),
        width=sidebar_width / 2 - 20,
        fg_color="#003049",
        hover_color="#000000",
        image=window.textures["import"],
        command=maze.import_file,
    )
    importButton.pack(side="left", padx=10, pady=(50, 0))

    # Export button
    exportButton.configure(
        text="Exporter",
        font=("Arial", 15),
        width=sidebar_width / 2 - 20,
        fg_color="#003049",
        hover_color="#000000",
        image=window.textures["export"],
        command=maze.export_file,
    )
    exportButton.pack(side="right", padx=10, pady=(50, 0))

    # Personalize button
    personalizeButton.configure(
        text="Pack de textures",
        font=("Arial", 15),
        width=sidebar_width - 20,
        image=window.textures["personalize"],
        fg_color="#7B2CBF",
        hover_color="#5A189A",
        command=maze.personalize,
    )
    personalizeButton.pack(side="bottom", pady=(0, 25))


def resize():
    maze.resize(grid_size)


# affichage taille de la grille
def on_resize_slider(x):
    global grid_size
    grid_size = int(x)
    sizeSliderLabel.configure(
        text=f"Taille de la grille: {int(grid_size)}x{int(grid_size)}"
    )
