import os

from functions import maze, sidebar, window
from pynput import keyboard

# Current working directory
os.chdir(os.path.dirname(os.path.abspath(__file__)))


SIDEBAR_SIZE = 300
MAZE_SIZE = 800

window.init()
window.resize(SIDEBAR_SIZE + MAZE_SIZE, MAZE_SIZE)
maze.init(size=20, real_size=MAZE_SIZE, canvas_x=SIDEBAR_SIZE)
sidebar.init(width=SIDEBAR_SIZE)

with keyboard.Listener(on_press=maze.on_key_press) as listener:
    window.root.mainloop()
    listener.join()
    listener.start()
