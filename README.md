# Maze Solver

![GitLab](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)
![Python](https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white)
![GitLab Release](https://img.shields.io/gitlab/v/release/nsi-curie%2Fmazesolver?color=ff0000&style=for-the-badge)

Résolveur de labyrinthe en Python réalisé dans le cadre du concours de programmation des [Trophées NSI](https://www.trophees-nsi.fr/) édition 2024.

![Screenshot](https://i.imgur.com/kT3BMIm.png)

# Sommaire

-   [Fonctionnalités](#fonctionnalités)
-   [Captures d'écran](#captures-décran)
-   [Bibliothèques](#bibliothèques)
-   [Installation](#installation)
-   [Utilisation](#utilisation)
-   [Contributeurs](#contributeurs)
-   [Licence](#licence)

# Fonctionnalités

-   Résolution automatique de labyrinthe
-   Résolution manuelle de labyrinthe (jeu)
-   Génération de labyrinthes
-   Packs de textures
-   Importation / Exportation de labyrinthes

# Captures d'écran

Voici quelques captures d'écran des différents packs de textures disponibles.

<ins>Jardin:</ins>

<a href="https://i.imgur.com/kT3BMIm.png"><img src="https://i.imgur.com/kT3BMIm.png" width="400"></a>

<ins>Château:</ins>

<a href="https://i.imgur.com/76PlVvp.png"><img src="https://i.imgur.com/76PlVvp.png" width="400"></a>

<ins>Volcan:</ins>

<a href="https://i.imgur.com/nGOltmp.png"><img src="https://i.imgur.com/nGOltmp.png" width="400"></a>

<ins>Banquise:</ins>

<a href="https://i.imgur.com/j9yFyqA.png"><img src="https://i.imgur.com/j9yFyqA.png" width="400"></a>

<ins>Quartier:</ins>

<a href="https://i.imgur.com/R3hjk4C.png"><img src="https://i.imgur.com/R3hjk4C.png" width="400"></a>

# Bibliothèques

-   [Tkinter](https://docs.python.org/3/library/tkinter.html) / [CustomTkinter](https://customtkinter.tomschimansky.com/)
-   [Pillow](https://pillow.readthedocs.io/en/stable/)
-   [Numpy](https://numpy.org/)
-   [Pynput](https://pynput.readthedocs.io/en/latest/)

# Installation

```bash
git clone https://gitlab.com/nsi-curie/MazeSolver.git
cd MazeSolver
pip install -r requirements.txt
```

# Utilisation

```bash
python sources
```

**⚠️ Ordinateurs Portables :** Assurez-vous d'avoir une mise à l'échelle de 100% pour éviter les problèmes d'affichage.

# Contributeurs

-   [Alex Lorenzi](https://github.com/eric-sasaoui)
-   [Natan Chiodi](https://gitlab.com/natoune)

Élèves de Première NSI au [lycée Pierre et Marie Curie](https://www.lycee-pierre-marie-curie.fr/) de Menton.

# Licence

Ce projet est sous licence [GNU GPL v3](LICENSE.md).
